# sxidle
## Simple X idle
Execute command after specified amount of user (X server) inactivity. Optionaly run another command while the first run is still running. 

## Uses
simple building block, screen lock

## What can it do
Just run it in background and it will do exactly what the description says!

**SYNOPSIS:** `sxidle [ -c condition_cmd ] time cmd [ period pcmd ]`
- command `cmd` is executed only if `condition_cmd` exited successfuly
- `time` and `period` are in minutes

To invoke it **directly** (without countdown), send `SIGUSR1` signal to `sxidle` process, example below.

Note: `cmd` (and optionaly `condition_cmd` and `pcmd`) will be invoked via `sh -c`

## Dependencies
[`xssstate`](https://suckless.org/xssstate)
'xorg-xset`

## Example usage
With provided utility scripts - `isfullscreen`, `smartsuspend` and [`slock`](https://tools.suckless.org/slock/) to provide **screen locker** solution.

```sh
# run this at boot, put it for ex. in your ~/.xinitrc (assuming all utilities in PATH)
xset s off -dpms  # turn default screensaver off
sxidle -c "! isfullscreen" 8 "xset dpms force off; slock" 5 smartsuspend &
```
- after **8 minutes** of inactivity turn off screen and lock it with `slock` **ONLY IF** there is nothing running in full screen mode (eg. a video player)
- while the screen is locked, every **10 minutes** check power supply; if running on battery, suspend
	- it runs periodically in case you resume from suspension but do not unlock the screen completely

- it should be noted that `isfullscreen` is working in `dwm` WM and I did not test it on other WMs.
```sh
# lock immedieately
pkill -SIGUSR1 sxidle
```
- `sxidle` can be informed to lock the screen immedieately by sending it a `SIGUSR1` signal

Sadly, the condition cannot be inserted like `sxidle 10 "isfullscreen || slock" 5 smartsuspend`, because it would cause suspend to run every loop when in fullscreen after idle.

Alternatively, it could be stated like `sxidle "isfullscreen || slock '{ while true; do; smartsuspend; sleep 5m; done; }'"`, but it is a little wordy and specific to `slock` (it runs whatever command supplied as argument as long as the `slock` itself is running).

## KNOWN BUGS
- time delay is a little longer due to sleep in main loop
